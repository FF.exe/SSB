Artificial Gravity Station
==========================

.. video:: W4L2sIYR_3M
   :frame: 4:01

4:01
----

**Stratish:** 
`My power is null.
An influence I once had is corrupt.
The consequences for disappointment are all but certain death.`

**Link:** `53o59gy <https://imgur.com/53o59gy>`_ - Imgur Link in the clear.

53o59gy
-------

    [csrss] has gone completely mad. What has happened I have absolutely no clue.
    
    
    Whatever the case, this process is now rogue. Obwiously, it doesn't have the ability to
    start destroying the system quite yet, but it's going to look for ways to start.
    
    I would reccomend we begin to monitor everything. Keep logs of all activity. If
    there are messages being passed between csrss and othe files, they need to be
    seen as quickly as possible, I want everyone pitching in on this.
    
    Also, I don't know if anyone has noticed, but a couple of .bat files had seemingly
    random bytes in them replaced. I was lucky enough to witness a
    0111 0000 turn into 1110 0000 0001 0010, which is an unassinged private
    character. I consulted [eudcedit] and found it looks like this:
    
    

    █▀█▀▀▀▀█
      ▀▀▀█ █
         █ █
         ▀▀█
          ▀▀
    
    
    Make no mistake, we have a very serious problem on our hands. please work to
    find out what's going on with [csrss] and resolve this as soon as possible.
    
    
    -[taskmgr]
    
This was used to find the glyph for the :doc:`/stratish` letter R.
