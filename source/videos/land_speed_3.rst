Land Speed Record 3
====================

.. video:: itr6vGHEYRE
   :frame: 2:14

2:14
----

**Stratish:** `The process cannot hurt me, but it can communicate and make me known. Its mind is weak, consumed with conviction. Perhaps it can be fooled.`_

**Link:** `bjMFJaS <https://imgur.com/bjMFJaS>`_ - Imgur Link in the clear.

bjMFJaS
-------

A heavaliy corrupted version of the thumbnail for :doc:`videos/eve_infinity`. It was used as a hint for decoding :doc:`/stratish`.

.. admonition:: TODO
   :class: danger

   Add image + Translation.

